<%-- 
    Document   : product
    Created on : Jun 21, 2023, 10:44:20 PM
    Author     : THIS PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <section class="product col-md-10">
            <!-- First row -->
            <div class="row">
                <!-- First Product -->
                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Second product -->
                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Third product -->
                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Second row -->
            <div class="row">
                <!-- First Product -->
                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Second product -->
                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Third product -->
                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Third row -->
            <div class="row">
                <!-- First Product -->
                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Second product -->
                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Third product -->
                <div class="col-lg-4 mb-md-5 ">
                    <div class="card h-100">
                        <img src="https://dummyimage.com/450x300/dee2e6/6c757d.jpg" alt="..." class="card-img-top">
                        <div class="card-body">
                            <div class="text-center">
                                <h5 class="card-title">Fancy Product</h5>
                                $40.00 - $80.00
                            </div>
                        </div>

                        <div class="card-footer  bg-transparent border-top-0">
                            <div class="text-center">
                                <a href="#" class="btn btn-outline-dark">View option</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
