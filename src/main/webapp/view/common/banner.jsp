<%-- 
    Document   : banner
    Created on : Jun 21, 2023, 10:50:35 PM
    Author     : THIS PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <section id="banner" class="container-fluid">
            <h1>Shop in style</h1>
            <p>With this shop homepage template</p>
        </section>
    </body>
</html>
