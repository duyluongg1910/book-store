<%-- 
    Document   : navbar
    Created on : Jun 21, 2023, 10:41:27 PM
    Author     : THIS PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <section id="navbar">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a href="#" class="navbar-brand">Start bootstrap</a>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="" class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">Feature</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">Pricing</a>
                </li>
            </ul>
            <form class="form-inline ml-auto mr-lg-3">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            <button class="btn btn-outline-dark mr-lg-3">
                <i class="fa-solid fa-cart-shopping"></i>&nbsp;&nbsp;&nbsp;Cart
            </button>
            <button class="btn btn-outline-primary">
                Login
            </button>
        </nav>
    </section>
    </body>
</html>
